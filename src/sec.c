#include <math.h>
#include <float.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "../include/platform.h"
#include "../include/task.h"


Platform *sort_platform(Platform *platform){

  //Primero obtenemos el número de dispositivos para poder crear un array auxiliar donde iremos colocando los dispositivos de forma ordenada
   int numDispositivos = platform->n_devices;

   Platform *auxiliar = (Platform *) malloc(sizeof(Platform));

   auxiliar->n_devices = numDispositivos;
   auxiliar->devices = (Device *) malloc(numDispositivos*sizeof(Device));
  memcpy(auxiliar->devices, platform->devices,numDispositivos* sizeof(Device));
  
   //Una vez tenemos el array auxiliar recorremos la lista de dispositivos ordenando en torno a la eficiencia (Consumo / instruccion)
 Device device_auxiliar;
  //Método bubblesort en principio, de forma correcta
   for (int i = 0; i<numDispositivos;i++){
 
     for(int j = 0; j < numDispositivos-1-i; j++){
       double consumoAnterior = auxiliar->devices[j].consumption/auxiliar->devices[j].inst_core;
       double consumoActual = auxiliar->devices[j + 1].consumption/auxiliar->devices[j +1].inst_core;

       if (consumoAnterior<consumoActual){
         device_auxiliar = auxiliar->devices[j + 1];
         auxiliar->devices[j + 1] = auxiliar->devices[j];
         auxiliar->devices[j] = device_auxiliar;
       }
     }      

   }
   
 /*for (int i = 0; i<numDispositivos;i++){
   printf("%d", auxiliar->devices[i].id);
 }*/
 return auxiliar;


}



int criterio(int nivel,int* s,int* executed, Task *tasks){
    //Deberemos comprobar que la tarea no tiene dependencias, si las tiene devolveremos falso(0) si no, devolveremos true(1)
    int tarea = s[nivel];
    int dependencias = tasks[tarea].n_deps;

    for (int i = 0; i < dependencias; i++)
	{
		if (executed[tasks[tarea].dep_tasks[i]] == 0)
			return 0;
	}
  return 1;

}

int retroceder(int nivel, int *s, int *executed){
  //Reiniciamos los contadores y devolvemos un nivel menos
  executed[s[nivel]] = 0;
   s[nivel] = -1;
  return nivel-1;


}

void generar(int nivel, int* s, int* executed){
  //Generamos el arbol, si no se encuentra el s[nivel] como -1 significa que ya hemos visitado esta rama asi que hay que ponerla a cero
  if (s[nivel] != -1)
		executed[s[nivel]] = 0;

//Mientras no encuentre una que ya se haya ejecutado, vamos sumando hasta dar con una tarea no ejecutada
	
	 s[nivel]++;
	while(executed[s[nivel]] == 1){
    s[nivel]++;
  }
    
  
 // printf("%d \n",s[nivel]);
	executed[s[nivel]] = 1;


}

int masHermanos(int nivel, int n_tasks, int *s, int *executed){
  //Debemos recorrer desde el nodo en el que nos encontramos hacia delante para comprobar si hay más hermanos que se puedan ejecutar en este nivel, solo miramos los de delante ya que los anteriores se deberían haber comprobado en el generar
  int i = s[nivel]+1
 for ( i ; i < n_tasks; i++){
		if (executed[i] == 0)
			return 1;
 }
	return 0;
}

//Si estamos en el final y se cumple el criterio, tenemos una solución
int solucion(int nivel,int n_task, int *s,int *executed, Task *tasks){
  
  return (nivel == n_task-1) && criterio(nivel, s, executed, tasks);


}



void get_solution(Task *tasks, int n_tasks, Platform *platform, Task *sorted_tasks, Platform *selected_dev, double *t, double *e)
{
    int i, ok;
    int nivel;
    double toa, coa;
    toa = coa = DBL_MAX;
    
    // Solucion Actual
    int *s = (int *) malloc(n_tasks*sizeof(int));
    assert(s);
    
    // Array Solucion Optima Actual
    int *soa = (int *) malloc(n_tasks*sizeof(int));
    assert(soa);
    
    // Array Tareas Ejecutadas
    int *executed = (int *) malloc(n_tasks*sizeof(int));
    assert(executed);
    
    // Inicializar Arrays
    memset(s, -1, sizeof(int)*n_tasks);
    memset(soa, -1, sizeof(int)*n_tasks);
    memset(executed, 0, sizeof(int)*n_tasks);
    
    // Ordenar dispositivos de menor a mayor consumo/instruccion TERMINADO
    Platform *sorted_platform = sort_platform(platform);
  
  
    nivel = 0;
    do {
        generar(nivel, s, executed);
        
        printf("Soy la tarea %d \n", s[nivel]);
        /*
        //Si es solución realizamos la asignación de dispositivos
        if (solucion(nivel, n_tasks, s, executed, tasks)) {
            // Obtener Asignacion de Dispositivos a Secuencia de Tareas
	    ok = assign_devices(s, n_tasks, tasks, sorted_platform, selected_dev, &toa, &coa);
	    
            if (ok) { // Actualizar Solucion
		memcpy(soa, s, sizeof(int)*n_tasks);
		*t = toa;
		*e = coa;
            }
        }*/
        //Si cumple el criterio y no hemos llegado al final avanzamos el nivel
        if (criterio(nivel, s, executed, tasks) && (nivel < n_tasks-1)) { 
          printf("Cumplo el criterio \n");
          nivel++;
        
        
         } 
        //Si no, comprobamos si hay más hermanos, y si no hay, retrocedemos nivel
        else {
            while (!masHermanos(nivel, n_tasks, s, executed) && (nivel > -1)) {
                nivel = retroceder(nivel, s, executed);
                   printf("Retrocedo\n");
            }
        }
       
    } while(nivel > -1);
    
    /*for(int i=0; i<n_tasks; i++) {
        sorted_tasks[i] = tasks[soa[i]];
    }
    
    // Imprimir mejor solucion
    print(sorted_tasks, selected_dev, t, e);
    
    free(s);
    free(soa);
    free(executed);
    free(sorted_platform->devices);
    free(sorted_platform);*/
}
